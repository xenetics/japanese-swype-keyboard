package com.example.lachlanmiller.ime;

import android.content.Context;

import java.util.Hashtable;

public class Hiragana {

    public Hashtable<String, String> hiragana = new Hashtable<String, String>();
    Context mContext;

    public Hiragana(Context c) {
        this.mContext = c;
        setupHiragana();

    }

    private void setupHiragana() {

        String hira = "あ,い,う,え,お,か,き,く,け,こ,さ,し,す," +
                "せ,そ,た,ち,つ,て,と,な,に,ぬ,ね,の,は,ひ,ふ,へ,ほ,ま," +
                "み,む,め,も,や,ゆ,よ,ら,り,る,れ,ろ,わ,を,ん";
        String[] hiraganaAll = hira.split(",");

        String roma = "A,I,U,E,O,KA,KI,KU,KE,KO," +
                "SA,SI,SU,SE,SO,TA,TI,TU,TE,TO,NA,NI,NU,NE,NO," +
                "HA,HI,HU,HE,HO,MA,MI,MU,ME,MO,YA,YU,YO," +
                "RA,RI,RU,RE,RO,WA,WO,N";

        String[] romanjiAll = roma.split(",");

        for (int i = 0; i < hiraganaAll.length; i++) {
            hiragana.put(romanjiAll[i], hiraganaAll[i]);
        }
    }

    public String toDakuon(String character) {
        switch (character.charAt(0)) {
            case 'K':
            switch (character.charAt(1)) {
                case 'A':
                    return "が";
                case 'I':
                    return "ぎ";
                case 'U':
                    return "ぐ";
                case 'E':
                    return "げ";
                case 'O':
                    return "ご";
            }
        }
        return "ER";
    }

    /* dakuon */

    public static char GA = 'が';
    public static char GI = 'ぎ';
    public static char GU = 'ぐ';
    public static char GE = 'げ';
    public static char GO = 'ご';

    public static char ZA = 'ざ';
    public static char ZI = 'じ';
    public static char ZU = 'ず';
    public static char ZE = 'ぜ';
    public static char ZO = 'ぞ';

    public static char DA = 'だ';
    public static char DI = 'ぢ';
    public static char DU = 'づ';
    public static char DE = 'で';
    public static char DO = 'ど';

    public static char BA = 'ば';
    public static char BI = 'び';
    public static char BU = 'ぶ';
    public static char BE = 'べ';
    public static char BO = 'ぼ';

    /* handakuon */

    public static char PA = 'ぱ';
    public static char PI = 'ぴ';
    public static char PU = 'ぷ';
    public static char PE = 'ぺ';
    public static char PO = 'ぽ';

    /* small characters */

    public static char LA = 'ぁ';
    public static char LI = 'ぃ';
    public static char LU = 'ぅ';
    public static char LE = 'ぇ';
    public static char LO = 'ぉ';
    public static char LTU = 'っ';

}

