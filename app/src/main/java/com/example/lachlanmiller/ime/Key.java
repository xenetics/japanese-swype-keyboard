package com.example.lachlanmiller.ime;

import android.graphics.Rect;
import android.widget.TextView;

/**
 * Created by lachlanmiller on 23/05/2016.
 */
public class Key {

    Rect mBounds;
    String mLetter;
    TextView mKeyTextView;

    public Key(TextView keyTextView, Rect bounds, String letter) {
        this.mBounds = bounds;
        this.mLetter = letter;
        this.mKeyTextView = keyTextView;
    }
}
