package com.example.lachlanmiller.ime;

import android.graphics.Rect;
import android.inputmethodservice.InputMethodService;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputConnection;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


/**
 * Created by lachlanmiller on 24/04/2016.
 */
public class Tester extends InputMethodService {

    Hiragana hiragana = null;
    final String TAG      = "kbdebug";
    final String TYPE_TAG = "typedebug";
    String buf = "";
    String previousKana = "";
    Key[] keys;

    @Override
    public View onCreateInputView() {

        final LinearLayout kb = (LinearLayout)getLayoutInflater().inflate(R.layout.keyboard, null);
        initKeyboard(kb);

        kb.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int x = (int) event.getX();
                int y = (int) event.getY();

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        Log.i(TAG, "Pressed down at x: " + str(asDpi(x)) + " y: " +  str(asDpi(y)));
                        for (Key key : keys) {
                            if (key != null) {
                                if (key.mBounds.contains(asDpi(x), asDpi(y))) {
                                    Log.i(TYPE_TAG, "Up on key containing " + key.mLetter);
                                    buf = key.mLetter;
                                }
                            }
                        }
                        return true;

                    case MotionEvent.ACTION_UP:
                        Log.i(TYPE_TAG, "Pressed up at x: " + str(asDpi(x)) + " y: " +  str(asDpi(y)));
                        for (Key key : keys) {
                            if (key != null) {
                                if (key.mBounds.contains(asDpi(x), asDpi(y))) {
                                    Log.i(TYPE_TAG, "Up on key containing " + key.mLetter);
                                    if (key.mLetter == buf) {
                                        // nothing! they didn't slide their finger.
                                    }
                                    else {
                                        buf += key.mLetter;
                                    }
                                    sendCharacterToScreen(buf);
                                    buf = "";
                                }
                            }
                        }
                        return true;
                }
                return true;
            }
        });
        return kb;
    }

    private String str(int i) {
        return String.valueOf(i);
    }

    private void sendCharacterToScreen(String character) {
        InputConnection ic = getCurrentInputConnection();

        String convertToHiragana = hiragana.hiragana.get(character);
        if (convertToHiragana != null) {
            previousKana = character;
            ic.commitText(convertToHiragana, 1);
        }
        else {
            Log.i(TAG, "Hiragana was null.");
            if (character.charAt(0) == '<') {
                // backspace!
                ic.deleteSurroundingText(1, 0);
            }
            if (character.charAt(0) == '"') {
                String withTenTen = hiragana.toDakuon(previousKana);
                ic.deleteSurroundingText(1, 0);
                ic.commitText(withTenTen, 1);
            }
        }
    }

    private void initKeyboard(final LinearLayout kb) {

        hiragana = new Hiragana(getApplicationContext());
        kb.post(new Runnable() {
            @Override
            public void run() {
                final LinearLayout ll = (LinearLayout)kb.getChildAt(0);
                ll.post(new Runnable() {
                    @Override
                    public void run() {
                        final TextView tv = (TextView)kb.findViewById(R.id.unique_1);
                        tv.post(new Runnable() {
                            @Override
                            public void run() {
                                Log.i(TAG, "Height-> " + str(asDpi(tv.getWidth())) + " Width-> " + str(asDpi(tv.getHeight())));
                                int count = 0;
                                keys = new Key[32];
                                for (int i = 0; i < kb.getChildCount(); i++) {
                                    LinearLayout line = (LinearLayout)kb.getChildAt(i);
                                    Log.i(TAG, "Line " + str(i) + " has: " + str(line.getChildCount()) + " children. Setting up keys for line " + str(i));
                                    for (int j = 0; j < line.getChildCount(); j++) {
                                        TextView k = (TextView)line.getChildAt(j);
                                        int top, left, right, bottom;
                                        left = asDpi(k.getLeft());
                                        top = i * asDpi(k.getHeight());
                                        bottom = (i+1) * asDpi(k.getHeight());
                                        right = asDpi(k.getRight());
                                        String s = k.getText() != null ? k.getText().toString() : "??";
                                        Key key = new Key(k, new Rect(left, top, right, bottom), s);
                                        Log.i(TAG, "-> " + " width:"+str(asDpi(k.getWidth())) + " height:"+str(asDpi(k.getHeight())) +
                                                " Rect (left->right, top->bottom): " + str(left) + "->" + str(right) + ", " + str(top) + "->" + str(bottom));
                                        keys[count] = key;
                                        count++;
                                    }
                                }
                            }
                        });
                    }
                });
            }
        });
    }

    public int asDpi(int loc) {
        float density = getResources().getDisplayMetrics().density;
        return (int)(loc/density);
    }
}
